package Data;
import java.util.ArrayList;
public abstract class Penduduk {
    private String nama;
    private String tempatTanggalLahir;
    //private ArrayList<Penduduk> anggota;

    public Penduduk() {
    }

    public Penduduk(String dataNama, String dataTempatTanggalLahir) {
        this.nama = dataNama;
        this.tempatTanggalLahir = dataTempatTanggalLahir;
    }
    public String getNama() {
        return nama;
    }
    public void setNama(String dataNama) {
        this.nama = dataNama;
    }
    public String getTempatTanggalLahir() {
        return tempatTanggalLahir;
    }
    public void setTempatTanggalLahir(String dataTempatTanggalLahir) {
        this.tempatTanggalLahir = dataTempatTanggalLahir;
    }
    
    public abstract double hitungIuran();
}