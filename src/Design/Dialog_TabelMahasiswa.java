package Design;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import Data.Mahasiswa;
import Data.Penduduk;
public class Dialog_TabelMahasiswa extends JDialog implements ActionListener {

    private JTable jt;
    private JScrollPane sp;
    private Mahasiswa mhs;
    private JLabel label_title;
    private JLabel label_nim;
    private JLabel label_nama;
    private JLabel label_ttl;
    private JTextField text_nim;
    private JTextField text_nama;
    private JTextField text_ttl;
    private JTextArea area_nama;

    public Dialog_TabelMahasiswa() {
        init();
    }

    public void init() {
        this.setLayout(null);
       label_title = new JLabel("List Mahasiswa");
        label_title.setBounds(110, 10, 200, 20);
        this.add(label_title);

        label_nama = new JLabel("Nama\t :");
        label_nama.setBounds(80, 60, 100, 50);
        this.add(label_nama);
        String asu = "";
        System.out.println(Main_Source.anggota.length);
        for (int i = 0; i < Main_Source.anggota.length; i++) {
            if (Main_Source.anggota[i] != null) {
                asu = asu + " " + Main_Source.anggota[i].getNama();
            }

        }
        area_nama = new JTextArea(asu);
        area_nama.setBounds(180, 75, 180, 20);
        this.add(area_nama);
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

}
